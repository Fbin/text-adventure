﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class Inventory
    {

        public static Inventory globalInstance;
        public List<string> items;

        public Inventory()
        {
            items = new List<string>();
        }

        public static Inventory GetGlobalInventory()
        {
            if (globalInstance == null)
            {
                globalInstance = new Inventory();
            }

            return globalInstance;
        }

        public int CountItems()
        {
            int count = items.Count;
            return count;
        }

        public void AddItem(string name)
        {
            items.Add(name);
        }

        public void RemoveItem(string name)
        {
            int i;
            while ((i = items.IndexOf(name)) >= 0)
            {
                items.RemoveAt(i);
            }
        }

        public bool HasItem(string name)
        {
            return items.Contains(name);
        }

        public void DisplayItems()
        {
            for (int x = 0; x < items.Count; x++)
            {
                Console.WriteLine(items[x]);
            }
        }
    }
}
