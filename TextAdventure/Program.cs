﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the advetures of Arnold Flad. He just returned from university. In front of his room, he realises that he's missing his keys.");
            Console.WriteLine("Where could he have lost them? At the 'university', while shopping at the 'grocery store' or when he took the shortcut throught the 'park'?");
            string choice = Console.ReadLine();
            Start start = new Start();
            start.firstchoice(choice);
        }
    }
}
