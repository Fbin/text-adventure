﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class Arnold
    {
        public int attack;
        public int defense;
        public int hp;
        string name;
        bool alive;

        public Arnold()
        {
            hp = 30;
            attack = 5;//+item;
            defense = new Random().Next(6, 9);
            alive = true;
        }

        public void def()
        {
            hp = hp - Choices.park.enemies.dog.dmg;
        }
    }
}
