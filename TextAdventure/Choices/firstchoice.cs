﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class Start
    {
        public Arnold anie = new Arnold();
        public void firstchoice(string choice)
        {
            switch (choice)
            {
                case "university":
                    Console.WriteLine("Arnold heads back for the university. As he arrives, he hears some noices at the film design room.");
                    Console.WriteLine("Shall he go looking for the reason of the 'noice' or should he stick to his plan and search at the 'game design room'?");
                    string continiue = Console.ReadLine();
                    Choices.atUniversity go = new Choices.atUniversity();
                    go.gouniversity(continiue);
                    break;
                case "grocery store":
                    Console.WriteLine("In front of the grocery store, Arnold Flad remembers meeting up with Chackie Chun in front of the potatoes.");
                    Console.WriteLine("Shall he go looking for the keys at the 'potatoes' or ask the 'shop clerk' if someone returned them?");
                    string continiue2 = Console.ReadLine();
                    Choices.store go1 = new Choices.store();
                    go1.gostore(continiue2);
                    break;
                case "park":
                    Console.WriteLine("As Arnold enters the park, he sees a magpie. Since the keys are glittering in the sunlight, the magpie might have taken them back to its nest.");
                    Console.WriteLine("Shall he trie to 'climb the tree' or should he 'keep looking on the ground'?");
                    string continiue3 = Console.ReadLine();
                    Choices.park go2 = new Choices.park();
                    go2.gopark(continiue3);
                    break;
                default:
                    Console.WriteLine("The keys can't be left at this place, if Arnold Flad was't there. Please choose something reasonable.");
                    string redo = Console.ReadLine();
                    firstchoice(redo);
                    break;
            }
        }
    }
}
